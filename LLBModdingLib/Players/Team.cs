﻿using System;
using LLBML.Utils;

namespace LLBML.Players
{
    /// <summary>
    /// A wrapper to the game's obfuscated Team enum
    /// </summary>
    public class Team : EnumWrapper<BGHNEHPFHGC>
    {
#pragma warning disable 1591
        Team(int id) : base(id) { }
        Team(BGHNEHPFHGC team) : base((int)team) { }

        public override string ToString()
        {
            return ((Enum)this.id).ToString();
        }

        public static implicit operator BGHNEHPFHGC(Team ew) => (BGHNEHPFHGC)ew.id;
        public static implicit operator Team(int id) => new Team(id);
        public static implicit operator Enum(Team ew) => (Enum)ew.id;
        public static implicit operator Team(Enum val) => new Team((int)val);
        public static implicit operator Team(BGHNEHPFHGC val) => new Team(val);

        #region wrapper
        public enum Enum
        {
            RED,
            BLUE,
            YELLOW,
            GREEN,
            NONE,
        }
        public enum Enum_Mappings
        {
            RED = BGHNEHPFHGC.EHPJJADIPNG,
            BLUE = BGHNEHPFHGC.PDOIGCCJJEO,
            YELLOW = BGHNEHPFHGC.JAGOLMCFMHD,
            GREEN = BGHNEHPFHGC.LHCCOPMFNOC,
            NONE = BGHNEHPFHGC.NMJDMHNMDNJ,
        }

        public static readonly Team RED = Enum.RED;
        public static readonly Team BLUE = Enum.BLUE;
        public static readonly Team YELLOW = Enum.YELLOW;
        public static readonly Team GREEN = Enum.GREEN;
        public static readonly Team NONE = Enum.NONE;
        #endregion
#pragma warning restore 1591
    }
}
