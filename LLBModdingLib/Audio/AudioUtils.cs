﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using BepInEx.Logging;
using BepInEx;

namespace LLBML.Audio
{
    /// <summary>
    /// A class containing helper methods for audio related tasks
    /// </summary>
    public static class AudioUtils
    {
        private static readonly ManualLogSource Logger = LLBMLPlugin.Log;
        private const string audioLogPrefix = "[Audio]: ";

        /// <summary>
        /// Loads an <see cref="AudioClip"/> synchronously
        /// </summary>
        /// <remarks>
        /// Prefer using an <see cref="AudioCache"/> to load clips as it is a very slow operation, potentially blocking execution.
        /// </remarks>
        /// <seealso cref="AudioCache"/>
        /// <param name="musicFilePath">The path of the audio file to load</param>
        /// <returns>The loaded <see cref="AudioAsset"/></returns>
        public static AudioAsset GetAssetSynchronously(string musicFilePath)
        {
            Logger.LogInfo(audioLogPrefix + "Urgent File Load: " + musicFilePath);
            AudioInfo audioInfo = AudioInfo.GetAudioInfo(musicFilePath);
            using (UnityWebRequest uwr = UnityWebRequestMultimedia.GetAudioClip(Utility.ConvertToWWWFormat(audioInfo.file.FullName), audioInfo.type))
            {
                try
                {
                    uwr.SendWebRequest();
                }
                catch (Exception e)
                {
                    Logger.LogError(audioLogPrefix + "MusicHax: Exception caught: ");
                    Logger.LogError(audioLogPrefix + e);
                }
                while (!uwr.isDone)
                {
                    Thread.Sleep(20);
                }
                AudioClip audioClip = DownloadHandlerAudioClip.GetContent(uwr);

                if (audioClip == null)
                {
                    Logger.LogError(audioLogPrefix + "Audioclip " + musicFilePath + " was null");
                }
                else
                {
                    audioClip.name = audioInfo.clipNameHint;
                    return new AudioAsset(audioClip, audioInfo);
                }
            }
            return null;
        }

        /// <inheritdoc cref="GetAssetSynchronously(string)"/>
        /// <returns>The loaded <see cref="AudioClip"/></returns>
        public static AudioClip GetClipSynchronously(string musicFilePath)
        {
            return GetAssetSynchronously(musicFilePath).audioClip;
        }

        /// <summary>
        /// Searches a directory for audio files
        /// </summary>
        /// <param name="dir">The path of the directory to search</param>
        /// <returns>A list of gathered <see cref="AudioInfo"/> in the directory</returns>
        public static List<AudioInfo> GetAudioInfos(DirectoryInfo dir)
        {
            List<AudioInfo> audioFiles = new List<AudioInfo>();
            FileInfo[] musicFiles = dir.GetFiles();
            foreach(FileInfo file in musicFiles)
            {
                AudioInfo audioInfo = AudioInfo.GetAudioInfo(file);
                if (audioInfo.type != AudioType.UNKNOWN)
                {
                    audioFiles.Add(audioInfo);
                }
            }
            return audioFiles;
        }

    }
}
