﻿using System;
using System.Collections.Generic;
using HarmonyLib;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using LLBML.Utils;

namespace LLBML
{
    /// <summary>
    /// The main plugin class for setting up all the lib needs.
    /// </summary>
    /// <remarks>
    /// As a user of the library, you shouldn't need anything in there. It needs to be public for BepInEx though =)
    /// </remarks>
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInProcess("LLBlaze.exe")]
    public class LLBMLPlugin : BaseUnityPlugin
    {
        internal static LLBMLPlugin Instance { get; private set; } = null;
        internal static ManualLogSource Log { get; private set; } = null;
        internal static ConfigFile Conf { get; private set; } = null;
        internal static bool StartReached { get; private set; } = false;

        private void Awake()
        {
            Instance = this;
            Log = Logger;
            Logger.LogInfo("Hello, world!");
            ConfigTypeConverters.AddConfigConverters();
            ModdingFolder.InitConfig(this.Config);

            var harmoInstance = new Harmony(PluginInfos.PLUGIN_ID);
            Messages.MessageApi.Patch(harmoInstance);
            Networking.NetworkApi.Patch(harmoInstance);
            GameEvents.GameEvents.PatchAll(harmoInstance);
            States.PlayerLobbyState.Patch(harmoInstance);
        }

        private void Start()
        {
            ModDependenciesUtils.RegisterToModMenu(this.Info, new List<string> {
                "The modding folder is the place where mods that use it will store user information." +
                "Things like sounds, skins or special configuration should get gathered there.",
                "For it to be applied properly, you should restart your game after changing it."
            });
            StartReached = true;

            BepInRef.RefCheck();
        }

        private void OnGUI()
        {
            LoadingScreen.OnGUI();
        }
    }
}
