﻿using System;

namespace LLBML.Math
{
    /// <summary>
    /// A wrapper to the game's obfuscated Floatf struct
    /// </summary>
    public struct Floatf
    {
#pragma warning disable 1591
        private readonly HHBCPNCDNDH _floatf;

        public Floatf(HHBCPNCDNDH f)
        {
            this._floatf = f;
        }
        public Floatf(decimal n)
        {
            this._floatf = DecimalToFloatf(n);
        }
        public Floatf(int i)
        {
            this._floatf = IntToFloatf(i);
        }
        public Floatf(float f)
        {
            this._floatf = FloatToFloatf(f);
        }
        public Floatf(double d)
        {
            this._floatf = DoubleToFloatf(d);
        }
        public Floatf(long l)
        {
            this._floatf = LongToFloatf(l);
        }

        public override bool Equals(object obj)
        {
            return (obj is HHBCPNCDNDH || obj is Floatf) && Equals((HHBCPNCDNDH)obj, (HHBCPNCDNDH)this);
        }
        public override int GetHashCode()
        {
            return _floatf.GetHashCode();
        }

        public static Floatf Sqrt(Floatf f) => HHBCPNCDNDH.PGBDOLPHANE(f);
        public override string ToString()
        {
            return Floatf.ToDecimal(this).ToString();
        }
        public string ToString(string format)
        {
            return Floatf.ToDecimal(this).ToString(format);
        }

        public static decimal ToDecimal(HHBCPNCDNDH f)
        {
            return f.EPOACNMBMMN / 4294967296m;
        }
        public static long ToLong(HHBCPNCDNDH f)
        {
            return f.EPOACNMBMMN >> 32;
        }
        public static float ToFloat(HHBCPNCDNDH f)
        {
            return (float)f.EPOACNMBMMN / 4.2949673E+09f;
        }
        public static double ToDouble(HHBCPNCDNDH f)
        {
            return (double)f.EPOACNMBMMN / 4294967296.0;
        }
        public static int FloorToInt(HHBCPNCDNDH f)
        {
            return HHBCPNCDNDH.HGDAIHMEFKC(f);
            //return (int)((getInnerLong(f) & -4294967296L) >> 32);
        }
        public static HHBCPNCDNDH DecimalToFloatf(decimal n) => HHBCPNCDNDH.NKKIFJJEPOL(n);
        public static HHBCPNCDNDH IntToFloatf(int i) => HHBCPNCDNDH.NKKIFJJEPOL(i);
        public static HHBCPNCDNDH LongToFloatf(long l) => HHBCPNCDNDH.GEIMAIJIPKI(l);
        public static HHBCPNCDNDH FloatToFloatf(float f) => HHBCPNCDNDH.GEIMAIJIPKI(f);
        public static HHBCPNCDNDH DoubleToFloatf(double d) => HHBCPNCDNDH.GEIMAIJIPKI(d);

        public static Floatf one { get { return HHBCPNCDNDH.GNIKMEGGCEP; } }
        public static Floatf zero { get { return HHBCPNCDNDH.DBOMOJGKIFI; } }
        public static Floatf Pi { get { return HHBCPNCDNDH.ICMHOBBMHHF; } }
        public static Floatf Deg2Rad { get { return HHBCPNCDNDH.OBFOGADMFFJ; } }
        public static Floatf Rad2Deg { get { return HHBCPNCDNDH.MJLJNMIAIFK; } }

        public static implicit operator HHBCPNCDNDH(Floatf a) => a._floatf;
        public static implicit operator decimal(Floatf a) => ToDecimal(a);
        public static implicit operator long(Floatf a) => ToLong(a);
        public static implicit operator float(Floatf a) => ToFloat(a);
        public static explicit operator double(Floatf a) => ToDouble(a);

        public static implicit operator Floatf(HHBCPNCDNDH a) => new Floatf(a);
        public static implicit operator Floatf(decimal a) => new Floatf(a);
        public static implicit operator Floatf(int a) => new Floatf(a);
        public static implicit operator Floatf(long a) => new Floatf(a);
        public static implicit operator Floatf(float a) => new Floatf(a);
        public static explicit operator Floatf(double a) => new Floatf(a);

        public static Floatf operator +(Floatf a) => a;

        public static HHBCPNCDNDH Negative(HHBCPNCDNDH a) => HHBCPNCDNDH.GANELPBAOPN(a);
        public static Floatf Negative(Floatf a) => HHBCPNCDNDH.GANELPBAOPN(a);
        public static Floatf operator -(Floatf a) => Negative(a);

        public static HHBCPNCDNDH Add(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.GAFCIOAEGKD(a, b);
        public static Floatf Add(Floatf a, Floatf b) => HHBCPNCDNDH.GAFCIOAEGKD(a, b);
        public static Floatf operator +(Floatf a, Floatf b) => Add(a._floatf, b._floatf);

        public static HHBCPNCDNDH Subtract(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.FCKBPDNEAOG(a, b);
        public static Floatf Subtract(Floatf a, Floatf b) => HHBCPNCDNDH.FCKBPDNEAOG(a, b);
        public static Floatf operator -(Floatf a, Floatf b) => Subtract(a._floatf, b._floatf);

        public static HHBCPNCDNDH Multiply(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.AJOCFFLIIIH(a, b);
        public static Floatf Multiply(Floatf a, Floatf b) => HHBCPNCDNDH.AJOCFFLIIIH(a, b);
        public static Floatf operator *(Floatf a, Floatf b) => Multiply(a._floatf, b._floatf);

        public static HHBCPNCDNDH Divide(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.FCGOICMIBEA(a, b);
        public static Floatf Divide(Floatf a, Floatf b) => HHBCPNCDNDH.FCGOICMIBEA(a, b);
        public static Floatf operator /(Floatf a, Floatf b) => Divide(a._floatf, b._floatf);

        public static HHBCPNCDNDH Modulus(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.CFIEILGNOBP(a, b);
        public static Floatf Modulus(Floatf a, Floatf b) => HHBCPNCDNDH.CFIEILGNOBP(a, b);
        public static Floatf operator %(Floatf a, Floatf b) => Modulus(a._floatf, b._floatf);

        public static bool Equals(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.ODMJDNBPOIH(a, b);
        public static bool Equals(Floatf a, Floatf b) => HHBCPNCDNDH.ODMJDNBPOIH(a, b);
        public static bool operator ==(Floatf a, Floatf b) => Equals(a._floatf, b._floatf);

        public static bool NotEquals(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.EAOICALCHJI(a, b);
        public static bool NotEquals(Floatf a, Floatf b) => HHBCPNCDNDH.EAOICALCHJI(a, b);
        public static bool operator !=(Floatf a, Floatf b) => NotEquals(a._floatf, b._floatf);

        public static bool GreaterThan(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.OAHDEOGKOIM(a, b);
        public static bool GreaterThan(Floatf a, Floatf b) => HHBCPNCDNDH.OAHDEOGKOIM(a, b);
        public static bool operator >(Floatf a, Floatf b) => GreaterThan(a._floatf, b._floatf);

        public static bool LesserThan(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.HPLPMEAOJPM(a, b);
        public static bool LesserThan(Floatf a, Floatf b) => HHBCPNCDNDH.HPLPMEAOJPM(a, b);
        public static bool operator <(Floatf a, Floatf b) => LesserThan(a._floatf, b._floatf);

        public static bool GreaterThanEquals(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.OCDKNPDIPOB(a, b);
        public static bool GreaterThanEquals(Floatf a, Floatf b) => HHBCPNCDNDH.OCDKNPDIPOB(a, b);
        public static bool operator >=(Floatf a, Floatf b) => GreaterThanEquals(a._floatf, b._floatf);

        public static bool LesserThanEquals(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.OILKPMKKNAK(a, b);
        public static bool LesserThanEquals(Floatf a, Floatf b) => HHBCPNCDNDH.OILKPMKKNAK(a, b);
        public static bool operator <=(Floatf a, Floatf b) => LesserThanEquals(a._floatf, b._floatf);

        public static Floatf Min(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.ANJPNFDPHFP(a, b);
        public static Floatf Min(Floatf a, Floatf b) => HHBCPNCDNDH.ANJPNFDPHFP(a, b);

        public static Floatf Max(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.BBGBJJELCFJ(a, b);
        public static Floatf Max(Floatf a, Floatf b) => HHBCPNCDNDH.BBGBJJELCFJ(a, b);

        public static Floatf ATan2(HHBCPNCDNDH a, HHBCPNCDNDH b) => HHBCPNCDNDH.GBGDEABPILN(a, b);
        public static Floatf ATan2(Floatf a, Floatf b) => HHBCPNCDNDH.GBGDEABPILN(a, b);

#pragma warning restore 1591

        #region Daio's additions =)

        /// <summary>
        /// Returns the time duration for an amount of frames assuming 60 fps.
        /// </summary>
        /// <returns>The duration at 60fps.</returns>
        /// <param name="frames">Number of frames.</param>
        public static Floatf FramesDuration60fps(int frames)
        {
            return (frames * (Floatf)(1f/60f)) - (HHBCPNCDNDH.PDKFDOPFPDO * 2m);
        }

        /// <summary>
        /// Returns the number of frames at 60 fps contained in a duration
        /// </summary>
        /// <returns>The number of frames.</returns>
        /// <param name="f">the duration.</param>
        public static float Time60Frames(Floatf f) => (float)f / World.DELTA_TIME;
        /// <inheritdoc cref="Time60Frames(Floatf)"/>
        public float ToTime60Frames() => Time60Frames(this);

        public static float PixelStandard(Floatf f) => (float)f * 100;
        /// <inheritdoc cref="PixelStandard(Floatf)"/>
        public float ToPixelStandard() => PixelStandard(this);

        public static float Pixels(Floatf f, bool convert = true) => convert ? (float)f / World.PIXEL60_SIZE : (float)f;
        public float ToPixels(bool convert = true) => Pixels(this, convert);

        public static float BUnits(Floatf f) => (float)f * World.PIXEL60_SIZE * 100;
        public float ToBUnits() => BUnits(this);


        public static float Value(Floatf f, bool isPixel) => isPixel ? PixelStandard(f) : BUnits(f);
        public float ToValue(bool isPixel) => Value(this, isPixel);

        #endregion
    }
}
