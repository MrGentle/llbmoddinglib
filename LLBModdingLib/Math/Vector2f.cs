﻿using System;
using UnityEngine;

namespace LLBML.Math
{
    /// <summary>
    /// A reimplementation of the game's obfuscated Vector2f struct
    /// </summary>
    public struct Vector2f
    {
#pragma warning disable 1591
        public Floatf x;
        public Floatf y;

        public Vector2f(Floatf _x, Floatf _y)
        {
            this.x = _x;
            this.y = _y;
        }

        public Vector2f(int _x, int _y)
        {
            this.x = (Floatf)_x;
            this.y = (Floatf)_y;
        }

        public static implicit operator IBGCBLLKIHA(Vector2f v) => new IBGCBLLKIHA(v.x, v.y);
        public static implicit operator Vector2f(IBGCBLLKIHA v) => new Vector2f(v.GCPKPHMKLBN, v.CGJJEHPPOAN);
        public static Vector2f operator +(Vector2f a, Vector2f b) => new Vector2f(a.x + b.x, a.y + b.y);

        public static Vector2f operator -(Vector2f a) => new Vector2f(-a.x, -a.y);
        public static Vector2f operator -(Vector2f a, Vector2f b) => new Vector2f(a.x - b.x, a.y - b.y);

        public static bool operator ==(Vector2f lhs, Vector2f rhs) => lhs.x == rhs.x && lhs.y == rhs.y;
        public static bool operator !=(Vector2f lhs, Vector2f rhs) => lhs.x != rhs.x || lhs.y != rhs.y;

        public static Vector2f operator *(Floatf d, Vector2f a) => new Vector2f(d * a.x, d * a.y);
        public static Vector2f operator *(Vector2f a, Floatf d) => new Vector2f(d * a.x, d * a.y);

        public static Vector2f operator /(Vector2f a, Floatf d) => new Vector2f(a.x / d, a.y / d);


        public static explicit operator Vector2f(Vector2 v) => new Vector2f((Floatf)v.x, (Floatf)v.y);
        public static explicit operator Vector2f(Vector3 v) => new Vector2f((Floatf)v.x, (Floatf)v.y);
        public static explicit operator Vector2f(Vector2i v) => new Vector2f(v.x, v.y);
        public static explicit operator Vector2(Vector2f v) => new Vector2((float)v.x, (float)v.y);
        public static explicit operator Vector3(Vector2f v) => new Vector3((float)v.x, (float)v.y, 0f);
        public static explicit operator Vector2i(Vector2f v) => new Vector2i(Mathf.RoundToInt((float)v.x), Mathf.RoundToInt((float)v.y));

        public Floatf magnitude // KEMFCABCHLO
        {
            get
            {
                if (this.x > (Floatf)30000m || this.y > (Floatf)30000m || this.x <  -(Floatf)30000m || this.y <  -(Floatf)30000m)
                {
                    return Floatf.Sqrt(this.x / (Floatf)10000m * this.x + this.y / (Floatf)10000m * this.y) * (Floatf)100m;
                }
                return Floatf.Sqrt(this.x * this.x + this.y * this.y);
            }
        }
        public Floatf sqrMagnitude { get { return this.x * this.x + this.y * this.y; } }


        public Vector2f normalized
        {
            get
            {
                Floatf mag = this.magnitude;
                if (mag == Floatf.zero)
                {
                    return Vector2f.zero;
                }
                return this / mag;
            }
        }

        public override bool Equals(object obj) => (obj is Vector2f || obj is IBGCBLLKIHA) && this.Equals((Vector2f)obj);

        public bool Equals(Vector2f v) => v.x == this.x && v.y == this.y;

        public override int GetHashCode() => this.x.GetHashCode() + this.y.GetHashCode();

        public static Vector2f one { get { return new Vector2f(Floatf.one, Floatf.one); } }
        public static Vector2f right { get { return new Vector2f(Floatf.one, Floatf.zero); } }
        public static Vector2f left { get { return new Vector2f(-Floatf.one, Floatf.zero); } }
        public static Vector2f up { get { return new Vector2f(Floatf.zero, Floatf.one); } }
        public static Vector2f down { get { return new Vector2f(Floatf.zero, -Floatf.one); } }
        public static Vector2f zero { get { return default(Vector2f); } }

        public override string ToString()
        {
            return string.Concat(new string[]
            {
            "(",
            this.x.ToString(),
            ", ",
            this.y.ToString(),
            ")"
            });
        }

        public string ToString(string format)
        {
            return string.Concat(new string[]
            {
            "(",
            this.x.ToString(format),
            ", ",
            this.y.ToString(format),
            ")"
            });
        }
#pragma warning restore 1591
        #region Daio's additions =)

        /// <summary>
        /// Converts a Direction to an Angle
        /// </summary>
        /// <param name="dir">Direction e.g flyDirection</param>
        /// <returns>Angle in degrees.</returns>
        public static Floatf DirectionToAngle(Vector2f dir) => 
            (Floatf.ATan2(dir.y, -dir.x) + Floatf.Pi) * Floatf.Rad2Deg;
        /// <inheritdoc cref="DirectionToAngle(Vector2f)"/>
        public Floatf ToAngle() => DirectionToAngle(this);
        #endregion
    }
}
