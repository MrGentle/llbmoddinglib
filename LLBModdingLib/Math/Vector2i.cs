﻿using System;
using System.Text;
using UnityEngine;

namespace LLBML.Math
{
    /// <summary>
    /// A reimplementation of the game's obfuscated Vector2i struct
    /// </summary>
    public struct Vector2i
    {
#pragma warning disable 1591
        public int x;
        public int y;

        public Vector2i(int _x, int _y)
        {
            this.x = _x;
            this.y = _y;
        }

        public static implicit operator JKMAAHELEMF(Vector2i v) => new JKMAAHELEMF(v.x, v.y);
        public static implicit operator Vector2i(JKMAAHELEMF v) => new Vector2i(v.GCPKPHMKLBN, v.CGJJEHPPOAN);
        public static explicit operator Vector2(Vector2i v) => new Vector2((float)v.x, (float)v.y);
        public static explicit operator Vector2i(Vector2 v) => new Vector2i((int)v.x, (int)v.y);

        public static Vector2i zero { get { return new Vector2i(0, 0); } }
        public static Vector2i one { get { return new Vector2i(1, 1); } }
        public static Vector2i up { get { return new Vector2i(0, 1); } }
        public static Vector2i down { get { return new Vector2i(0, -1); } }

        public static bool operator ==(Vector2i v1, Vector2i v2) => v1.x == v2.x && v1.y == v2.y;
        public static bool operator !=(Vector2i v1, Vector2i v2) => v1.x != v2.x || v1.y != v2.y;

        public static Vector2i operator +(Vector2i v1, Vector2i v2) => new Vector2i(v1.x + v2.x, v1.y + v2.y);

        public static Vector2i operator -(Vector2i v1) => new Vector2i(-v1.x, -v1.y);
        public static Vector2i operator -(Vector2i v1, Vector2i v2) => new Vector2i(v1.x - v2.x, v1.y - v2.y);

        public static Vector2i operator *(Vector2i v, int f) => new Vector2i(v.x * f, v.y * f);
        public static Vector2i operator *(int f, Vector2i v) => v * f;
        public static Vector2i operator *(Vector2i v1, Vector2i v2) => new Vector2i(v1.x * v2.x, v1.y * v2.y);

        public override bool Equals(object obj)
        {
            Vector2i vector2i = (Vector2i)obj;
            return this.x == vector2i.x && this.y == vector2i.y;
        }

        public override int GetHashCode()
        {
            int num = 17;
            num = num * 23 + this.x.GetHashCode();
            return num * 23 + this.y.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append('(').Append(this.x).Append(", ").Append(this.y).Append(')');
            return stringBuilder.ToString();
        }

#pragma warning restore 1591
    }
}
