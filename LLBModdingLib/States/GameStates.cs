﻿using System;
using System.Collections.Generic;
using System.Reflection;
using LLScreen;
using HarmonyLib;

namespace LLBML.States
{
    /// <summary>
    /// A wrapper to the game's obfuscated Player class
    /// </summary>
    public class GameStates
    {
#pragma warning disable 1591
        private readonly DNPFJHMAIBP _gameStates;

        public GameStates(DNPFJHMAIBP gs)
        {
            this._gameStates = gs;
        }
        public override bool Equals(object obj) => (obj is DNPFJHMAIBP || obj is GameStates) && Equals((DNPFJHMAIBP)obj, (DNPFJHMAIBP)this);
        public override int GetHashCode() => _gameStates.GetHashCode();
        public override string ToString() => _gameStates.ToString();
        public static implicit operator DNPFJHMAIBP(GameStates gs) => gs._gameStates;
        public static implicit operator GameStates(DNPFJHMAIBP gs) => new GameStates(gs);

        #region wrapper
        public static GameState GetCurrent() => DNPFJHMAIBP.HHMOGKIMBNM();
        public static GameStates Instance => DNPFJHMAIBP.AKGAOAEJ;
        public static void DirectProcess(Message message) => DNPFJHMAIBP.OLGPEGGAIIB(message);
        public static void DirectProcess(Msg msg, int playerNr, int index) => DirectProcess(new Message(msg, playerNr, index, null, -1));
        public static void ClearMessages() => DNPFJHMAIBP.CJLNFAABMGM();
        public static bool ShouldIgnoreInputs() => DNPFJHMAIBP.CNEEALIJJFE();
        public static bool IsInIntro() => DNPFJHMAIBP.DEDHLLMANNE();
        public static bool IsSwitching() => DNPFJHMAIBP.KHJPIEHGCHK();
        public static bool IsMenuState(GameState s, bool alsoOptions = false) => DNPFJHMAIBP.LJCBCBAKKDF(s, alsoOptions);
        public static void Send(Message message) => DNPFJHMAIBP.GKBNNFEAJGO(message);
        public static void Send(Msg msgType, int playerNr, int index) => DNPFJHMAIBP.GKBNNFEAJGO(msgType, playerNr, index);
        public static void SendAfter(float delay, Message message) => DNPFJHMAIBP.EPPPGGPNAPK(delay, message);
        public static void Set(GameState newState, bool noLink = false) => DNPFJHMAIBP.HOGJDNCMNFP(newState, noLink);
        //public static void Spawn(GameState startState) => DNPFJHMAIBP.LEAFDPPHKMK(startState);
        public static void UpdateToConfigAll() => DNPFJHMAIBP.CHILDIDHNBH();


        public static List<Message> Messages => DNPFJHMAIBP.IGEBDAFJIDI;
        public static FHPLAFJAAIP GetCurrentGameStateObject()
        {
            return DNPFJHMAIBP.AKGAOAEJ.EAPCPAJPEMA(GetCurrent());
        }
        #endregion wrapper
#pragma warning restore 1591

        #region additions
        /// <summary>
        /// Is the current state a lobby.
        /// </summary>
        /// <returns><c>true</c>, if curently in any type of lobby, <c>false</c> otherwise.</returns>
        public static bool IsInLobby() => IsInLocalLobby() || IsInOnlineLobby() || IsInSingleLobby();
        /// <summary>
        /// Is the current state a local lobby.
        /// </summary>
        /// <returns><c>true</c>, if curently in a local lobby, <c>false</c> otherwise.</returns>
        public static bool IsInLocalLobby() => GetCurrent() == GameState.LOBBY_LOCAL;
        /// <summary>
        /// Is the current state an online lobby.
        /// </summary>
        /// <returns><c>true</c>, if curently in an online lobby, <c>false</c> otherwise.</returns>
        public static bool IsInOnlineLobby() => GetCurrent() == GameState.LOBBY_ONLINE;
        /// <summary>
        /// Is the current state a single-player lobby.
        /// </summary>
        /// <returns><c>true</c>, if curently in a single-player lobby, <c>false</c> otherwise.</returns>
        public static bool IsInSingleLobby()
        {
            GameState currentGS = GetCurrent();
            return currentGS == GameState.LOBBY_TRAINING || currentGS == GameState.LOBBY_STORY
                || currentGS == GameState.LOBBY_TUTORIAL || currentGS == GameState.LOBBY_CHALLENGE;
        }
        /// <summary>
        /// Is the current state any state of game.
        /// </summary>
        /// <returns><c>true</c>, if curently in game, <c>false</c> otherwise.</returns>
        public static bool IsInGame()
        {
            GameState currentGS = GetCurrent();
            return (currentGS == GameState.GAME || currentGS == GameState.GAME_PAUSE);
        }
        /// <summary>
        /// Is the current state an ongoing game.
        /// </summary>
        /// <returns><c>true</c>, if curently in a match, <c>false</c> otherwise.</returns>
        public static bool IsInMatch()
        {
            GameState currentGS = GetCurrent();
            return World.instance != null && !UIScreen.loadingScreenActive && IsInGame();
        }
        #endregion additions
    }
}
