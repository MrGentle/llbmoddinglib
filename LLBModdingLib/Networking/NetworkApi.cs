﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Multiplayer;
using Steamworks;
using HarmonyLib;
using BepInEx.Logging;
using LLBML.Players;
using LLBML.Utils;

namespace LLBML.Networking
{
    internal enum SpecialCode
    {
        GREETINGS = 200,
        SUPER_DUPER_SECRET_CODE_THAT_IS_THE_CHOOSEN_ONE_AS_DESTINY_WANTED_IT_TO_BE_THAT_WAS_DESCRIBED_IN_SACRED_DISCORD_PRIVATE_MESSAGES = 204
    }

    public enum Channel : int
    {
        Vanilla = 0,
        ModPackets = 1,
        Transaction = 2,
    }

    public static class NetworkApi
    {
        private static ManualLogSource Logger = LLBMLPlugin.Log;

        internal const int newMessageBufferSize = 32768;
        internal const int maxSteamworksReliablePacket = 1 * 1024 * 1024; // 1MB

        /// <summary>
        /// Gets a value indicating whether the current states are online.
        /// </summary>
        /// <value><c>true</c> if is online; otherwise, <c>false</c>.</value>
        public static bool IsOnline => JOMBNFKIHIC.GDNFJCCCKDM;
        /// <summary>
        /// Gets the current online mode.
        /// </summary>
        /// <value>The OnlineMode enum value for the current mode.</value>
        public static OnlineMode OnlineMode => JOMBNFKIHIC.EAENFOJNNGP;

        /// <summary>
        /// Gets the current player number for the local player.
        /// </summary>
        /// <remarks>Curently not failsafe, game has it as -1 in some occasions, and localPeer is initialized before the Player is</remarks>
        /// <returns>The current platform as a PlatformBase object.</returns>
        [Obsolete("Deprecated, please use Player.LocalPlayerNumber instead.")]
        public static int LocalPlayerNumber => P2P.localPeer?.playerNr ?? 0;

        /// <summary>
        /// Gets the current platform.
        /// </summary>
        /// <returns>The current platform as a PlatformBase object.</returns>
        public static KIIIINKJKNI GetCurrentPlatform() => CGLLJHHAJAK.GIGAKBJGFDI; //Platform.current
        /// <summary>
        /// Gets the operating system the game is currently running on.
        /// </summary>
        /// <returns>The operating system.</returns>
        public static OperatingSystem GetOperatingSystem() => CGLLJHHAJAK.EFECBCOGFHC; //Platform.operatingSystem


        public const int netIDSize = 4;

        public static byte[] GetNetworkIdentifier(string source)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(Encoding.ASCII.GetBytes(source));
            byte[] hash = md5.Hash;
            LLBMLPlugin.Log.LogDebug($"Hash for {source} = {hash}, length: {hash.Length}");
            byte[] reduced_hash = Math.BinaryUtils.XORFold(hash, 2);
            LLBMLPlugin.Log.LogDebug($"UID for {source} = {StringUtils.PrettyPrintBytes(reduced_hash)}, length: {reduced_hash.Length}");
            return reduced_hash;
        }

        public static void SendMessageToPlayer(int playerNr, Message message)
        {
            SendMessageToPeer(Player.GetPlayer(playerNr).peer.peerId, message);
        }

        public static void SendMessageToPeer(string peerId, Message message)
        {
            Envelope envelope = new Envelope
            {
                sender = P2P.localPeer.peerId,
                receiver = peerId,
                message = message,
                packetNr = -1
            };
            SendEnvelope(envelope);
        }

        public static void SendEnvelope(Envelope envelope)
        {
            P2P.localPeer.networkImplementation.Send(envelope);
        }
        public static void SendBytes(string receiver, byte[] data, bool toVanilla = true) 
        {
            if (data.Length > NetworkApi.newMessageBufferSize)
            {
                Logger.LogError($"Couldn't send byte array to {receiver} : buffer overflow ({data.Length})");
                return;
            }
            CSteamID steamIDRemote = PeerIDToCSteamID(receiver);
            EP2PSend eP2PSendType = EP2PSend.k_EP2PSendUnreliable;
            if (data.Length > 1024) eP2PSendType = EP2PSend.k_EP2PSendReliable;
            SendP2PPacket(steamIDRemote, data, eP2PSendType, (int)(toVanilla ? Channel.Vanilla : Channel.ModPackets));
        }


        public static string PlayerNrToPeerID(int playerNr)
        {
            return Player.GetPlayer(playerNr).peer.peerId;
        }

        public static CSteamID PeerIDToCSteamID(string peerId)
        {

            CSteamID steamID = KKMGLMJABKH.KJANLAGMBAM(peerId); // PlatformSteam.PeerToSteam(receiver);
            if (!steamID.OIAPLNLLNNL() || !steamID.BNAMNCNLOGD()) // steamIDRemote.IsValid() || !steamIDRemote.BIndividualAccount()
            {
                Logger.LogError($"Peer '{peerId} invalid ({steamID.AECOKNACKLO()})"); // steamIDRemote.GetEAccountType()
                throw new ArgumentException($"Peer '{peerId} invalid ({steamID.AECOKNACKLO()})");
            }
            return steamID;
        }


        public static void SendP2PPacket(Player player, byte[] data, EP2PSend.Enum eP2PSendType = EP2PSend.Enum.k_EP2PSendUnreliable, int nChannel = (int)Channel.Vanilla, bool transactionSupport = true)
        {
            CSteamID steamID = PeerIDToCSteamID(PlayerNrToPeerID(player.nr));
            SendP2PPacket(steamID, data, eP2PSendType, nChannel);
        }

        public static void SendP2PPacket(CSteamID steamIDRemote, byte[] data, EP2PSend.Enum eP2PSendType = EP2PSend.Enum.k_EP2PSendUnreliable, int nChannel = (int)Channel.Vanilla, bool transactionSupport = true)
        {
            if (data.Length > Transaction.MaxFragmentSize)
            {
                if (transactionSupport)
                {
                    Logger.LogDebug($"Creating transaction for packet because it's size exeeded max length by {data.Length - Transaction.MaxFragmentSize}");
                    Transaction.Create(steamIDRemote, data, nChannel).Start();
                    return;
                }
                else
                {
                    Logger.LogWarning($"Packet exeeded max size by {data.Length - Transaction.MaxFragmentSize}, but transaction support was turned off. Aborting send.");
                    return;
                }
            }

            if (!BEPOKFLHPBM.BEMEPOKJHGF(steamIDRemote, data, (uint)data.Length, (EP2PSend)eP2PSendType, nChannel)) // SteamNetworking.SendP2PPacket
            {
                Logger.LogError($"SendP2PPacket(Byte array to {steamIDRemote}) failed!");
                P2P.Disconnect(DisconnectReason.UNKNOWN_SEND_FAILED);
            }
        }

        /// <summary>
        /// Sends binary data in a special p2p channel. Receive data with <see cref="RegisterModPacketCallback(BepInEx.PluginInfo, Action{Peer, byte[]})"/> 
        /// </summary>
        /// <remarks>
        /// Maximum size is 1MB.
        /// </remarks>
        /// <param name="pluginInfo">The <see cref="BepInEx.PluginInfo"/> of the sending plugin</param>
        /// <param name="player">The player to send the packet to</param>
        /// <param name="data">The data to send</param>
        public static void SendModPacket(BepInEx.PluginInfo pluginInfo, Player player, byte[] data)
        {
            byte[] netID = GetNetworkIdentifier(pluginInfo.Metadata.GUID);
            byte[] payload = netID.AddRangeToArray(data);
            SendP2PPacket(player, payload, EP2PSend.k_EP2PSendReliable, (int)Channel.ModPackets);
        }

        /// <summary>
        /// Register a method to receive binary data sent by <see cref="SendModPacket(BepInEx.PluginInfo, Player, byte[])"/>
        /// </summary>
        /// <param name="pluginInfo">The <see cref="BepInEx.PluginInfo"/> of the requesting plugin</param>
        /// <param name="onReceivePacket">The method to call when receiving the packet</param>
        public static void RegisterModPacketCallback(BepInEx.PluginInfo pluginInfo, Action<Peer, byte[]> onReceivePacket)
        {
            byte[] netID = NetworkApi.GetNetworkIdentifier(pluginInfo.Metadata.GUID);
            string index = StringUtils.PrettyPrintBytes(netID);
            if (modPacketCallbacks.ContainsKey(index))
            {
                LLBMLPlugin.Log.LogWarning($"{pluginInfo.Metadata.Name} has already registered a callback.");
                return;
            }
            modPacketCallbacks.Add(index, onReceivePacket);
        }
        internal static Dictionary<string, Action<Peer, byte[]>> modPacketCallbacks = new Dictionary<string, Action<Peer, byte[]>>();


        internal static void Patch(Harmony harmonyPatcher)
        {
            harmonyPatcher.PatchAll(typeof(Networking_Patches.ReceiveEnvelopePatch));
            harmonyPatcher.PatchAll(typeof(Networking_Patches.CustomEnvelopeEncodingPatch));
            harmonyPatcher.PatchAll(typeof(Networking_Patches.MessageBufferSizePatch));
            harmonyPatcher.PatchAll(typeof(Networking_Patches.NetSteamPatch));
        }

    }
}
