﻿using System;
using System.Linq;
using System.Collections.Generic;
using HarmonyLib;
using Multiplayer;
using Steamworks;
using BepInEx.Logging;
using LLBML.Messages;
using LLBML.Utils;

namespace LLBML.Networking
{
#pragma warning disable 1591
    public static class Networking_Patches
    {
        private static ManualLogSource Logger = LLBMLPlugin.Log;

        public static class MessageBufferSizePatch
        {
            [HarmonyPatch(typeof(P2P), "get_messageBufferSize")]
            [HarmonyPostfix]
            public static int get_messageBufferSize_Postfix(int __result)
            {
                //Logger.LogDebug("Current game messageBufferSize: " + P2P.mMessageBufferSize + " . Returning " + MessageApi.newMessageBufferSize + " instead.");
                return NetworkApi.newMessageBufferSize;
            }
        }

        public static class NetSteamPatch
        {
            public static Dictionary<Channel, Action<byte[], uint, CSteamID>> channelMapping = new Dictionary<Channel, Action<byte[], uint, CSteamID>>
            {
                [Channel.ModPackets] = ReceiveModPacket,
                [Channel.Transaction] = ReceiveTransaction,
            };

            [HarmonyPatch(typeof(NetSteam), nameof(NetSteam.DoUpdate))]
            [HarmonyPrefix]
            public static bool DoUpdate_Prefix(NetSteam __instance)
            {
                foreach(var channelMap in channelMapping)
                {
                    while (BEPOKFLHPBM.IAGFGJEKDNF(out uint packetSize, (int)channelMap.Key))
                    {
                        byte[] packetBuffer = new byte[packetSize];
                        bool success = BEPOKFLHPBM.FOPENCDPLGO(packetBuffer, packetSize, out uint bytesRead, out CSteamID cSteamID, (int)channelMap.Key);
                        if (success)
                        {
                            channelMap.Value.Invoke(packetBuffer, bytesRead, cSteamID);
                        }
                        else
                        {
                            Logger.LogError($"ReadP2PPacket(Byte array from {cSteamID}) failed!");
                            Logger.LogError($"Tried to read {packetSize} bytes on channel {channelMap.Key}.");
                            continue;
                        }
                    }
                }
                return true;
            }

            public static void ReceiveModPacket(byte[] data, uint bytesRead, CSteamID sender)
            {
                var netID = data.Take(NetworkApi.netIDSize).ToArray();
                string index = StringUtils.PrettyPrintBytes(netID);
                if (NetworkApi.modPacketCallbacks.ContainsKey(index))
                {
                    var onReceiveModPacket = NetworkApi.modPacketCallbacks[index];
                    try
                    {
                        onReceiveModPacket(Peer.Get(sender.ToString()), data.Skip(NetworkApi.netIDSize).ToArray());
                    }
                    catch (Exception e)
                    {
                        LLBMLPlugin.Log.LogError($"netID:{netID.ToString()} returned error during PlayerLobbyState unpacking: " + e);
                        return;
                    }
                }
                else
                {
                    Logger.LogWarning($"Unknown PluginID: Couldn't find a plugin with \"{StringUtils.PrettyPrintBytes(netID)}\" as netID. It had {bytesRead - NetworkApi.netIDSize} bytes of data.");
                    return;
                }
            }


            public static void ReceiveTransaction(byte[] data, uint bytesRead, CSteamID sender)
            {
                var netID = data.Take(NetworkApi.netIDSize).ToArray();
                string index = StringUtils.PrettyPrintBytes(netID);
            }
        }

        public static class ReceiveEnvelopePatch
        {
            [HarmonyPatch(typeof(LocalPeer), nameof(LocalPeer.OnReceiveMessage))]
            [HarmonyPrefix]
            public static bool OnReceiveMessage_Prefix(Envelope envelope, LocalPeer __instance)
            {
                Message message = envelope.message;
                ushort msgCode = (ushort)message.msg;
                if (msgCode <= 255)
                {
                    if (MessageApi.vanillaMessageCodes.Contains((byte)msgCode))
                    {
                        if (MessageApi.vanillaSubscriptions.ContainsKey((byte)msgCode))
                        {
                            Logger.LogDebug("Received vanilla message with custom subscriptions. Number: " + msgCode + ". Sender: " + envelope.sender);
                            foreach (CustomMessage customMessage in MessageApi.vanillaSubscriptions[(byte)msgCode])
                            {
                                customMessage.messageActions.onReceiveCode(message);
                            }
                        }
                        return true;
                    }
                    else
                    {
                        Logger.LogWarning("Received non-vanilla message in vanilla range. Number: " + msgCode + ". Sender: " + envelope.sender);
                        EnvelopeApi.OnReceiveCustomEnvelopeCall(__instance, new EnvelopeEventArgs(envelope));
                        return false;
                    }
                }

                if (MessageApi.customMessages.ContainsKey(msgCode) || MessageApi.internalMessageCodes.Contains(msgCode))
                {
                    Logger.LogDebug("Received custom message. Number: " + msgCode + ". Sender: " + envelope.sender);
                    MessageApi.customMessages[msgCode].messageActions.onReceiveCode(envelope.message);
                    return false;
                }

                EnvelopeApi.OnReceiveCustomEnvelopeCall(__instance, new EnvelopeEventArgs(envelope));
                Logger.LogWarning("Received unregistered custom message. Number: " + msgCode + ". Sender: " + envelope.sender);
                return false;
            }
        }

        public static class CustomEnvelopeEncodingPatch
        {
            [HarmonyPatch(typeof(Envelope), nameof(Envelope.ToBytes))]
            [HarmonyPrefix]
            public static bool ToBytes_Prefix(bool socketHeader, byte byte0, Envelope __instance, ref byte[] __result)
            {
                byte[] res = EnvelopeApi.WriteBytesEnvelope(__instance, SpecialCode.SUPER_DUPER_SECRET_CODE_THAT_IS_THE_CHOOSEN_ONE_AS_DESTINY_WANTED_IT_TO_BE_THAT_WAS_DESCRIBED_IN_SACRED_DISCORD_PRIVATE_MESSAGES,socketHeader, byte0);
                if (res != null)
                {
                    __result = new byte[res.Length];
                    res.CopyTo(__result, 0);
                    return false;
                }
                return true;
            }

            [HarmonyPatch(typeof(Envelope), nameof(Envelope.FromBytes))]
            [HarmonyPrefix]
            public static bool FromBytes_Prefix(byte[] bytes, bool socketHeader, ref Envelope __result)
            {
                try
                {
                    Envelope? result = EnvelopeApi.ReceiveEnvelope(bytes, socketHeader);
                    if (result.HasValue)
                    {
                        __result = result.Value;
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (Exception err)
                {
                    Logger.LogError($"Caught exception while reading Envelope: {err.Message}\n{err.StackTrace}");
                    
                }
                Logger.LogDebug("Sending byte form envelope to vanilla.");
                return true;
            }
        }
    }
#pragma warning restore 1591
}
