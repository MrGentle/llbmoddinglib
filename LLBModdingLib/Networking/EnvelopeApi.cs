﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Multiplayer;
using HarmonyLib;
using LLBML.Messages;
using BepInEx.Logging;

namespace LLBML.Networking
{
    public static class EnvelopeApi
    {
        private static ManualLogSource Logger = LLBMLPlugin.Log;

        public delegate void OnReceiveEnvelopeHandler(LocalPeer sender, EnvelopeEventArgs e);
        public static event OnReceiveEnvelopeHandler OnReceiveCustomEnvelope;


        internal static void OnReceiveCustomEnvelopeCall(LocalPeer sender, EnvelopeEventArgs e)
        {
            if (OnReceiveCustomEnvelope == null) return;
            OnReceiveCustomEnvelope(sender, e);
        }

        internal static byte[] WriteBytesEnvelope(Envelope envelope, SpecialCode specialCode = SpecialCode.SUPER_DUPER_SECRET_CODE_THAT_IS_THE_CHOOSEN_ONE_AS_DESTINY_WANTED_IT_TO_BE_THAT_WAS_DESCRIBED_IN_SACRED_DISCORD_PRIVATE_MESSAGES, bool socketHeader = false, byte byte0 = 0)      
        {
            ushort msgCode = (ushort)envelope.message.msg;

            if (MessageApi.customMessages.ContainsKey(msgCode) || MessageApi.internalMessageCodes.Contains(msgCode))
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (BinaryWriter binaryWriter = new BinaryWriter(memoryStream))
                    {
                        if (socketHeader)
                        {
                            binaryWriter.Write(byte0);
                            binaryWriter.Write(envelope.packetNr);
                        }
                        binaryWriter.Write(envelope.sender);
                        binaryWriter.Write(envelope.receiver);

                        binaryWriter.Write((byte)specialCode);
                        binaryWriter.Write((ushort)envelope.message.msg);
                        binaryWriter.Write(envelope.message.playerNr);
                        binaryWriter.Write(envelope.message.index);
                        if (MessageApi.customMessages[msgCode].messageActions.customWriter != null)
                        {
                            MessageApi.customMessages[msgCode].messageActions.customWriter(binaryWriter, envelope.message.ob, envelope.message.obSize);
                        }
                        else
                        {

                            if (envelope.message.ob is byte[] byteEncoded)
                            {
                                binaryWriter.Write(byteEncoded.Length);
                                binaryWriter.Write(byteEncoded);
                            }
                            else
                            {
                                binaryWriter.Write(-1);
                                if (envelope.message.ob != null)
                                {
                                    Logger.LogWarning("Got message with an invalid payload, either provide a custom writer or pass a byte[] as the message object");
                                }
                            }
                        }
                        return memoryStream.ToArray();
                    }
                }
            }
            return null;
        }

        internal static Envelope? ReceiveEnvelope(byte[] bytes, bool socketHeader)
        {
            Envelope result = default(Envelope);
            SpecialCode specialCode = SpecialCode.SUPER_DUPER_SECRET_CODE_THAT_IS_THE_CHOOSEN_ONE_AS_DESTINY_WANTED_IT_TO_BE_THAT_WAS_DESCRIBED_IN_SACRED_DISCORD_PRIVATE_MESSAGES;
            using (MemoryStream memoryStream = new MemoryStream(bytes))
            {
                using (BinaryReader binaryReader = new BinaryReader(memoryStream))
                {
                    if (socketHeader)
                    {
                        binaryReader.ReadByte();
                        result.packetNr = binaryReader.ReadInt32();
                    }
                    result.sender = binaryReader.ReadString();
                    result.receiver = binaryReader.ReadString();
                    specialCode = (SpecialCode)binaryReader.ReadByte();
                    if (!MessageApi.internalMessageCodes.Contains((int)specialCode))
                        return null;
                    ushort msgCode = binaryReader.ReadUInt16();
                    if (!MessageApi.customMessages.ContainsKey(msgCode))
                        return null;
                    result.message.msg = (Msg)msgCode;
                    result.message.playerNr = binaryReader.ReadInt32();
                    result.message.index = binaryReader.ReadInt32();


                    if (MessageApi.customMessages[msgCode].messageActions.customReader != null && specialCode == SpecialCode.SUPER_DUPER_SECRET_CODE_THAT_IS_THE_CHOOSEN_ONE_AS_DESTINY_WANTED_IT_TO_BE_THAT_WAS_DESCRIBED_IN_SACRED_DISCORD_PRIVATE_MESSAGES)
                    {
                        MessageApi.customMessages[msgCode].messageActions.customReader(binaryReader, result.message);
                    }
                    else
                    {
                        result.message.obSize = binaryReader.ReadInt32();
                        if (result.message.obSize != -1)
                        {
                            result.message.ob = binaryReader.ReadBytes(result.message.obSize);
                        }
                        else
                        {
                            result.message.ob = null;
                        }
                    }
                }
            }

            switch (specialCode)
            {
                case SpecialCode.SUPER_DUPER_SECRET_CODE_THAT_IS_THE_CHOOSEN_ONE_AS_DESTINY_WANTED_IT_TO_BE_THAT_WAS_DESCRIBED_IN_SACRED_DISCORD_PRIVATE_MESSAGES:
                    return result;
                default:
                    return result;
            }
        }
    }


    public class EnvelopeEventArgs : EventArgs
    {
        public Envelope Envelope { get; private set; }

        public EnvelopeEventArgs(Envelope envelope)
        {
            Envelope = envelope;
        }
    }
}
