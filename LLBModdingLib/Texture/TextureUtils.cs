﻿using System;
using System.IO;
using UnityEngine;
using BepInEx.Logging;
using B83.Image;
using DDSLoader;

namespace LLBML.Texture
{
    public static class TextureUtils
    {
        private static ManualLogSource Logger = LLBMLPlugin.Log;

        /// <summary>
        /// Loads a png from a file and returns it.
        /// </summary>
        /// <remarks>
        /// Loads the asset into memory, you should only load it once.
        /// Look into <see cref="TextureCache"/> if you need a class to manage that.
        /// </remarks>
        /// <returns>The loaded texture.</returns>
        /// <param name="file">The png file to load.</param>
        public static Texture2D LoadPNG(FileInfo file) 
        {
            if (!file.Exists)
            {
                Logger.LogWarning("LoadPNG: Could not find " + file.FullName);
                return null;
            }

            byte[] spriteBytes;
            using (var fileStream = file.OpenRead())
            using (var reader = new BinaryReader(fileStream))
            {
                var png = PNGTools.ReadPNGFile(reader);
                for (int i = png.chunks.Count - 1; i >= 0; i--)
                {
                    var c = png.chunks[i];
                    if (c.type == EPNGChunkType.gAMA ||
                        c.type == EPNGChunkType.pHYs ||
                        c.type == EPNGChunkType.sRBG)
                    {
                        png.chunks.RemoveAt(i);
                    }
                }
                using (var mem = new MemoryStream())
                {
                    PNGTools.WritePNGFile(png, new BinaryWriter(mem));
                    spriteBytes = mem.ToArray();
                }
            }

            string texName = Path.GetFileNameWithoutExtension(file.FullName);


            Texture2D tex = DefaultTexture();
            tex.name = texName;
            tex.LoadImage(spriteBytes); //This resizes the texture width and height

            return tex;
        }
        /// <inheritdoc cref="LoadPNG(FileInfo)"/>
        public static Texture2D LoadPNG(string filePath) => LoadPNG(new FileInfo(filePath));


        /// <summary>
        /// Loads a dds from a file and returns it.
        /// </summary>
        /// <remarks>
        /// Loads the asset into memory, you should only load it once.
        /// Look into <see cref="TextureCache"/> if you need a class to manage that.
        /// </remarks>
        /// <returns>The loaded texture.</returns>
        /// <param name="file">The dds file to load.</param>
        public static Texture2D LoadDDS(FileInfo file)
        {

            DDSImage dds = new DDSImage(File.ReadAllBytes(file.FullName));
            Texture2D tex = DefaultTexture(dds.Width, dds.Height, dds.GetTextureFormat());
            tex.LoadRawTextureData(dds.GetTextureData());
            tex.Apply();

            return (tex);
        }
        /// <inheritdoc cref="LoadDDS(FileInfo)"/>
        public static Texture2D LoadDDS(string filePath) => LoadDDS(new FileInfo(filePath));


        /// <summary>
        /// Loads a file and returns a texture from it.
        /// </summary>
        /// <remarks>
        /// Loads the asset into memory, you should only load it once.
        /// Look into <see cref="TextureCache"/> if you need a class to manage that.
        /// </remarks>
        /// <returns>The loaded texture.</returns>
        /// <param name="file">The file to load.</param>
        public static Texture2D LoadTexture(FileInfo file)
        {
            switch (file.Extension.ToLower())
            {
                case ".png": return LoadPNG(file);
                case ".dds": return LoadDDS(file);
                default: throw new NotSupportedException("Unkown file extension: " + file.Extension);
            }
        }
        /// <inheritdoc cref="LoadTexture(FileInfo)"/>
        public static Texture2D LoadTexture(string filePath) => LoadTexture(new FileInfo(filePath));

        /// <summary>
        /// Returns a default texture with optional size.
        /// </summary>
        /// <returns>The texture.</returns>
        /// <param name="width">New texture's width.</param>
        /// <param name="height">New texture's height.</param>
        /// <param name="format">New texture's format.</param>
        public static Texture2D DefaultTexture(int width = 512, int height = 512, TextureFormat format = TextureFormat.RGBA32)
        {
            return new Texture2D(width, height, format, true, false)
            {
                filterMode = FilterMode.Trilinear,
                anisoLevel = 9,
                mipMapBias = -0.5f,
            };
        }
    }
}
