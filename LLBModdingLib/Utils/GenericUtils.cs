﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LLBML.Utils
{
    /// <summary>
    /// Utils for generics or types operations.
    /// </summary>
    public static class GenericUtils
    {
        /// <summary>
        /// Gets enum values from the type via reflection.
        /// </summary>
        /// <typeparam name="T">The targeted enum type.</typeparam>
        /// <returns>The enum values.</returns>
        public static IEnumerable<T> GetEnumValues<T>() where T : new()
        {
            T valueType = new T();
            return typeof(T).GetFields()
                .Select(fieldInfo => (T)fieldInfo.GetValue(valueType))
                .Distinct();
        }
    }

    public static class ArrayExtension
    {
        public static T[] Add<T>(this T[] target, params T[] items)
        {
            // Validate the parameters
            if (target == null)
            {
                target = new T[] { };
            }
            if (items == null)
            {
                items = new T[] { };
            }

            // Join the arrays
            T[] result = new T[target.Length + items.Length];
            target.CopyTo(result, 0);
            items.CopyTo(result, target.Length);
            return result;
        }
    }
}
