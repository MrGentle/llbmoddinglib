﻿using System;
using System.Collections.Generic;
using System.Linq;
using LLHandlers;
using BepInEx.Logging;
using HarmonyLib;
using LLBML.Players;

namespace LLBML.Utils
{
    /// <summary>
    /// A class containing helper methods for debbuging game behaviors
    /// </summary>
    public static class DebugUtils
    {
        /// <summary>
        /// Makes a string from a decisions array sent by the host at a match start.
        /// </summary>
        /// <returns>The human readable decisions</returns>
        /// <param name="decisions">The decisions array</param>
        public static string DecisionsToString(int[] decisions)
        {

            string debugString = "\n";
            int dIndex = 0;
            debugString += dIndex + ":\tStage:\t\t" + (Stage)decisions[dIndex++] + "\n";
            debugString += dIndex + ":\tInputDelay:\t" + decisions[dIndex++] + "\n";
            debugString += dIndex + ":\tSeed:\t\t" + decisions[dIndex++] + "\n";
            for (int i = 0; i < 4; i++)
            {
                debugString += dIndex + ":\tP" + i + " CharSel\t" + (Character)decisions[dIndex++] + "\n";
                debugString += dIndex + ":\tP" + i + " Char\t\t" + (Character)decisions[dIndex++] + "\n";
                debugString += dIndex + ":\tP" + i + " Variant\t" + (CharacterVariant)decisions[dIndex++] + "\n";
                debugString += dIndex + ":\tP" + i + " Team\t\t" + (Team)decisions[dIndex++] + "\n";
                debugString += dIndex + ":\tP" + i + " IsSpec\t" + (decisions[dIndex++] == 1) + "\n";
            }
            for (int i = 0; i < HPNLMFHPHFD.ELPLKHOLJID.GEMMKJGHPPP(); i++)
            {
                debugString += dIndex + ":\t" +(GameSetting)i + ": " + decisions[dIndex++] + "\n";
            }
            if (dIndex < decisions.Length)
            {
                
                for (int i = 0; i < 4; i++)
                {
                    int aiLevel = decisions[dIndex++];
                    bool isAi = aiLevel != -1;
                    debugString += dIndex + ":\tP" + i + " IsAI\t\t" + isAi;
                    debugString += (isAi ? "\tAILevel\t" + aiLevel : "") + "\n";
                }
            }
            for (int i = 0; i < decisions.Length; i++)
            {

                LLBMLPlugin.Log.LogDebug(i +":\t"+ decisions[i]);
            }
            return debugString;
        }

        /// <summary>
        /// Prints the current stacktrace.
        /// </summary>
        public static void PrintStacktrace()
        {
            LLBMLPlugin.Log.LogInfo("---------------------------- Stacktrace: \n" +
                new System.Diagnostics.StackTrace());
        }
    }
}
