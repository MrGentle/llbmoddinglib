﻿using System;
using System.Linq;
using LLBML.Math;


namespace LLBML.Utils
{
    /// <summary>
    /// A wrapper to the game's obfuscated ControlledRandom static class
    /// </summary>
    public static class ControlledRandom
    {
#pragma warning disable 1591
        #region wrapper
        private static uint frame { get => FKPOFIAIEAD.IGNMBCNLHHI; set => FKPOFIAIEAD.IGNMBCNLHHI = value; }
        private static uint seed { get => FKPOFIAIEAD.FMIIELCJDNL; set => FKPOFIAIEAD.FMIIELCJDNL = value; }
        private static uint x { get => FKPOFIAIEAD.GCPKPHMKLBN; set => FKPOFIAIEAD.GCPKPHMKLBN = value; }
        private static uint y { get => FKPOFIAIEAD.CGJJEHPPOAN; set => FKPOFIAIEAD.CGJJEHPPOAN = value; }
        private static uint z { get => FKPOFIAIEAD.KEEABMIGLLF; set => FKPOFIAIEAD.KEEABMIGLLF = value; }
        private static uint w { get => FKPOFIAIEAD.HNAKLBBGADM; set => FKPOFIAIEAD.HNAKLBBGADM = value; }
        private static int filled { get => FKPOFIAIEAD.GKMBAELHGJP; set => FKPOFIAIEAD.GKMBAELHGJP = value; }
        private static int iresolution { get => FKPOFIAIEAD.FHIHEECJJNN; set => FKPOFIAIEAD.FHIHEECJJNN = value; }
        private static Floatf fresolution { get => FKPOFIAIEAD.NPINAIIJPDP; }
        private static Floatf[] prefilled { get => FKPOFIAIEAD.HLHHJDFBOHE.Cast<Floatf>().ToArray(); }

        public static void SetFrame(int frame) => FKPOFIAIEAD.BLHAAJFDOOB(frame);
        public static void SetSeed(uint seed) => FKPOFIAIEAD.GKFENAHMMBI(seed);
        private static void Reset() => FKPOFIAIEAD.FMCGJNPNIPH();
        private static Floatf GetNext() => FKPOFIAIEAD.POFICLEECNH();
        public static Floatf GetF(int index) => FKPOFIAIEAD.MEFPGLFENLM(index);
        public static Floatf GetF(int index, Floatf min, Floatf max) => FKPOFIAIEAD.MEFPGLFENLM(index, min, max);
        public static int Get(int index, int min, int max) => FKPOFIAIEAD.BJDPHEHJJJK(index, min, max);
        public static string GetState() => FKPOFIAIEAD.IANMMGNADPE();
        #endregion wrapper
#pragma warning restore 1591
    }
}
