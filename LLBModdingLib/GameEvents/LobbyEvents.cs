﻿using System;
using LLScreen;
using HarmonyLib;
using Multiplayer;
using LLBML.Players;

namespace LLBML.GameEvents
{
    /// <summary>
    /// Delegate for the <see cref="LobbyEvents.OnLobbyEntered"/> event.
    /// </summary>
    public delegate void OnLobbyEnteredHandler(object source, LobbyEventArgs e);
    /// <summary>
    /// Delegate for the <see cref="LobbyEvents.OnLobbyReady"/> event.
    /// </summary>
    public delegate void OnLobbyReadyHandler(object source, LobbyReadyArgs e);
    /// <summary>
    /// Delegate for the <see cref="LobbyEvents.OnUserCharacterPick"/> event.
    /// </summary>
    public delegate void OnUserCharacterPickHandler(PlayersCharacterButton source, OnUserCharacterPickArgs e);
    /// <summary>
    /// Delegate for the <see cref="LobbyEvents.OnUserCharacterPick"/> event.
    /// </summary>
    public delegate void OnUserSkinClickHandler(PlayersSelection source, OnUserSkinClickArgs e);
    /// <summary>
    /// Delegate for the <see cref="LobbyEvents.OnPlayerJoin"/> event.
    /// </summary>
    public delegate void OnPlayerJoinHandler(Player source, OnPlayerJoinArgs e);
    /// <summary>
    /// Delegate for the <see cref="LobbyEvents.OnUnlinkFromPlayer"/> event.
    /// </summary>
    public delegate void OnUnlinkFromPlayerHandler(Peer source, OnUnlinkFromPlayerArgs e);

    /// <summary>
    /// A class to hold various events related to lobby activity.
    /// </summary>
    public static class LobbyEvents
    {
        internal static void Patch(Harmony harmonyInstance)
        {
            harmonyInstance.PatchAll(typeof(LobbyEvents_Patches));
            OnUserCharacterPick += (o, a) =>
                LLBMLPlugin.Log.LogDebug("Event OnUserCharacterPick triggered. Player nr: " + a.playerNr + ". Char: " + a.character);
            OnUserSkinClick += (o, a) =>
                LLBMLPlugin.Log.LogDebug("Event OnUserSkinClick triggered. clicker nr: " + a.clickerNr + ". ToSkin nr: " + a.toSkinNr);
            OnLobbyEntered += (o, a) =>
                LLBMLPlugin.Log.LogDebug("Event OnLobbyEntered triggered. Lobby ID: " + a.lobby_id + ". Lobby Host: " + a.host_id);
            OnLobbyReady += (o, a) =>
                LLBMLPlugin.Log.LogDebug("Event OnLobbyReady triggered. Lobby ID: " + a.lobby_id + ". Lobby Host: " + a.host_id);
            OnPlayerJoin += (o, a) =>
                LLBMLPlugin.Log.LogDebug($"Event OnPlayerJoin triggered. Player nr: {a.playerNr}. It's a {(a.isLocal ? "local" : "remote")} player.");
            OnUnlinkFromPlayer += (o, a) =>
                    LLBMLPlugin.Log.LogDebug($"Event OnUnlinkFromPlayer triggered. Player nr: {a.playerNr}.");
        }

        /// <summary>
        /// Occurs when a lobby is entered.
        /// </summary>
        /// <remarks>
        /// Be aware that this happens very early in the process, with mostly only the p2p setted up.
        /// Use <see cref="OnLobbyReady"/> if you need a later event, when things like Player classes are ready.
        /// </remarks>
        public static event OnLobbyEnteredHandler OnLobbyEntered;
        internal static void OnLobbyEnteredCall(object source, LobbyEventArgs e)
        {
            if (OnLobbyEntered == null) return;
            OnLobbyEntered(source, e);
        }

        /// <summary>
        /// Occurs when a lobby has finished setting up and is ready for user use.
        /// </summary>
        public static event OnLobbyReadyHandler OnLobbyReady;
        internal static void OnLobbyReadyCall(object source, LobbyReadyArgs e)
        {
            if (OnLobbyReady == null) return;
            OnLobbyReady(source, e);
        }

        /// <summary>
        /// Occurs when a user picks a character.
        /// </summary>
        public static event OnUserCharacterPickHandler OnUserCharacterPick;
        internal static void OnUserCharacterPickCall(PlayersCharacterButton source, OnUserCharacterPickArgs e)
        {
            if (OnUserCharacterPick == null) return;
            OnUserCharacterPick(source, e);
        }

        /// <summary>
        /// Occurs when a user cycle the skins.
        /// </summary>
        public static event OnUserSkinClickHandler OnUserSkinClick;
        internal static void OnUserSkinClickCall(PlayersSelection source, OnUserSkinClickArgs e)
        {
            if (OnUserSkinClick == null) return;
            OnUserSkinClick(source, e);
        }

        /// <summary>
        /// Occurs when a player joins the lobby.
        /// </summary>
        public static event OnPlayerJoinHandler OnPlayerJoin;
        internal static void OnPlayerJoinCall(Player source, OnPlayerJoinArgs e)
        {
            if (OnPlayerJoin == null) return;
            OnPlayerJoin(source, e);
        }


        /// <summary>
        /// Occurs when a player leaves an online lobby.
        /// </summary>
        public static event OnUnlinkFromPlayerHandler OnUnlinkFromPlayer;
        internal static void OnUnlinkFromPlayerCall(Peer source, OnUnlinkFromPlayerArgs e)
        {
            if (OnUnlinkFromPlayer == null) return;
            OnUnlinkFromPlayer(source, e);
        }
    }

    /// <summary>
    /// Class to hold generic arguments for some events in <see cref="LobbyEvents"/>.
    /// </summary>
    public class LobbyEventArgs : EventArgs
    {
        /// <summary>
        /// Is it an online or local lobby.
        /// </summary>
        public bool isOnline { get; private set; }
        /// <summary>
        /// The game's ID for the lobby. <see langword="null"/> if offline.
        /// </summary>
        public string lobby_id { get; private set; }
        /// <summary>
        /// The game's ID for the lobby's host. <see langword="null"/> if offline.
        /// </summary>
        public string host_id { get; private set; }

        public LobbyEventArgs(bool isOnline, string lobby_id, string host_id)
        {
            this.lobby_id = lobby_id;
            this.host_id = host_id;
        }
    }


    /// <summary>
    /// Class to hold arguments for the <see cref="LobbyEvents.OnLobbyReady"/> event.
    /// </summary>
    public class LobbyReadyArgs : EventArgs
    {
        /// <summary>
        /// Is it an online or local lobby.
        /// </summary>
        public bool isOnline { get; private set; }
        /// <summary>
        /// The game's ID for the lobby. <see langword="null"/> if offline.
        /// </summary>
        public string lobby_id { get; private set; }
        /// <summary>
        /// The game's ID for the lobby's host. <see langword="null"/> if offline.
        /// </summary>
        public string host_id { get; private set; }

        public LobbyReadyArgs(bool isOnline, string lobby_id, string host_id)
        {
            this.lobby_id = lobby_id;
            this.host_id = host_id;
        }
    }

    /// <summary>
    /// Class to hold arguments for the <see cref="LobbyEvents.OnUserCharacterPick"/> event.
    /// </summary>
    public class OnUserCharacterPickArgs : EventArgs
    {
        /// <summary>
        /// The number of the user that clicked a character button.
        /// </summary>
        public int playerNr { get; private set; }
        /// <summary>
        /// The character picked by the user.
        /// </summary>
        public Character character { get; private set; }

        public OnUserCharacterPickArgs(int playerNr, Character character)
        {
            this.playerNr = playerNr;
            this.character = character;
        }
    }


    /// <summary>
    /// Class to hold arguments for the <see cref="LobbyEvents.OnUserSkinClick"/> event.
    /// </summary>
    public class OnUserSkinClickArgs : EventArgs
    {
        /// <summary>
        /// The number of the user that clicked a player model.
        /// </summary>
        public int clickerNr { get; private set; }
        /// <summary>
        /// The player that the new variant will be applied to. 
        /// </summary>
        public int toSkinNr { get; private set; }

        public OnUserSkinClickArgs(int clickerNr, int toSkinNr)
        {
            this.clickerNr = clickerNr;
            this.toSkinNr = toSkinNr;
        }
    }


    /// <summary>
    /// Class to hold arguments for the <see cref="LobbyEvents.OnPlayerJoin"/> event.
    /// </summary>
    public class OnPlayerJoinArgs : EventArgs
    {
        /// <summary>
        /// The id from the player that just joined.
        /// </summary>
        public int playerNr { get; private set; }
        /// <summary>
        /// Whether the player is a local one or not.
        /// </summary>
        public bool isLocal { get; private set; }

        public OnPlayerJoinArgs(int playerNr, bool isLocal)
        {
            this.playerNr = playerNr;
            this.isLocal = isLocal;
        }
    }

    /// <summary>
    /// Class to hold arguments for the <see cref="LobbyEvents.OnUnlinkFromPlayer"/> event.
    /// </summary>
    public class OnUnlinkFromPlayerArgs : EventArgs
    {
        /// <summary>
        /// The id from the player that just joined.
        /// </summary>
        public int playerNr { get; private set; }

        public OnUnlinkFromPlayerArgs(int playerNr)
        {
            this.playerNr = playerNr;
        }
    }
}
