﻿using System;
using HarmonyLib;
using LLBML.States;

namespace LLBML.GameEvents
{
    /// <summary>
    /// Delegate for the <see cref="GameStateEvents.OnStateChange"/> event.
    /// </summary>
    public delegate void OnStateChangeHandler(object source, OnStateChangeArgs e);

    /// <summary>
    /// A class to hold various events related to game states.
    /// </summary>
    public static class GameStateEvents
    {
        internal static void Patch(Harmony harmonyInstance)
        {
            harmonyInstance.PatchAll(typeof(GameStateEvents_Patches));
            OnStateChange += (o, a) =>
                LLBMLPlugin.Log.LogDebug("Event OnStateChange triggered. OldState: " + a.oldState + ". NewState: " + a.newState);
        }

        /// <summary>
        /// Occurs when the game state transition from one state to another.
        /// </summary>
        public static event OnStateChangeHandler OnStateChange;
        internal static void OnStateChangeCall(object source, OnStateChangeArgs e)
        {
            if (OnStateChange == null) return;
            OnStateChange(source, e);
        }
    }

    /// <summary>
    /// Class to hold arguments for the <see cref="GameStateEvents.OnStateChange"/> event.
    /// </summary>
    public class OnStateChangeArgs : EventArgs
    {
        /// <summary>
        /// The <see cref="GameState"/> that was active before this one.
        /// </summary>
        public GameState oldState { get; private set; }
        /// <summary>
        /// The <see cref="GameState"/> that will be active.
        /// </summary>
        public GameState newState { get; private set; }

        public OnStateChangeArgs(GameState oldState, GameState newState)
        {
            this.oldState = oldState;
            this.newState = newState;
        }
    }
}
