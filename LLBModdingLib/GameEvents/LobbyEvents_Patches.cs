﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Multiplayer;
using HarmonyLib;
using LLBML.States;
using LLBML.Players;
using Steamworks;

namespace LLBML.GameEvents
{
#pragma warning disable 1591
    public static class LobbyEvents_Patches
    {
        [HarmonyPatch(typeof(PlayersCharacterButton), "<Init>m__0")]
        [HarmonyPostfix]
        public static void PickChar_Postfix(int pNr, PlayersCharacterButton __instance)
        {
            LobbyEvents.OnUserCharacterPickCall(__instance, new OnUserCharacterPickArgs(pNr, __instance.character));
        }
        [HarmonyPatch(typeof(PlayersSelection), "<Init>m__4")]
        [HarmonyPostfix]
        public static void PickSkin_Postfix(int pNr, PlayersSelection __instance)
        {
            LobbyEvents.OnUserSkinClickCall(__instance, new OnUserSkinClickArgs(pNr, __instance.playerNr));
        }


        private static CSteamID lobby_CSteamID;
        [HarmonyPatch(typeof(KKMGLMJABKH), nameof(KKMGLMJABKH.OMANHANHHKC))] // PlatformSteam.OnLobbyEntered
        [HarmonyPostfix]
        public static void OnLobbyEntered_Postfix(KMILCAPIEHH PKEOMLCLPNE)
        {
            lobby_CSteamID = new CSteamID(PKEOMLCLPNE.CDPLMFKJIDK);
        }

        [HarmonyPostfix]
        [HarmonyPatch(typeof(HPNLMFHPHFD), nameof(HPNLMFHPHFD.CNewState))] // GameStatesLobby.CNewState
        public static IEnumerator CNewState_Postfix(IEnumerator orig, HPNLMFHPHFD __instance)
        {
            for (int c = 1; orig.MoveNext(); c++)
            {
                // if c == some number of steps, run your stuff
                yield return orig.Current; // Run original enumerator for one step
            }
            LobbyEventArgs args = new LobbyEventArgs(false, null, null); ;

            if (GameStates.IsInOnlineLobby())
            {
                args = new LobbyEventArgs(true, lobby_CSteamID.ToString(), Player.GetPlayer(0)?.peer?.peerId); // PlatformSteam.curLobby
            }
            else
            {
                args = new LobbyEventArgs(false, null, null);
            }
            LobbyEvents.OnLobbyEnteredCall(__instance, args);
            LLBMLPlugin.Instance.StartCoroutine(CallLobbyReady(__instance, args));
        }
        public static IEnumerator CallLobbyReady(HPNLMFHPHFD instance, LobbyEventArgs args)
        {
            yield return new WaitForSeconds(0.2f);
            if (GameStates.IsInLobby())
            {
                LobbyEvents.OnLobbyReadyCall(instance, new LobbyReadyArgs(args.isOnline, args.lobby_id, args.host_id ?? Player.GetPlayer(0)?.peer?.peerId));
            }
        }


        [HarmonyPatch(typeof(ALDOKEMAOMB), nameof(ALDOKEMAOMB.EKNFACPOJCM))] // Player.JoinMatch
        [HarmonyPostfix]
        public static void JoinMatch_Postfix(bool __0, ALDOKEMAOMB __instance)
        {
            bool join = __0;
            Player player = __instance;
            LobbyEvents.OnPlayerJoinCall(__instance, new OnPlayerJoinArgs(player.nr, player.isLocal));
        }

        [HarmonyPatch(typeof(Peer), nameof(Peer.UnlinkFromPlayer))]
        [HarmonyPostfix]
        public static void UnlinkFromPlayer_Postfix(Peer __instance)
        {
            LobbyEvents.OnUnlinkFromPlayerCall(__instance, new OnUnlinkFromPlayerArgs(__instance.playerNr));
        }


        public static void LogAllInstructions(IEnumerable<CodeInstruction> instructions)
        {
            int i = 0;
            foreach (var inst in instructions)
            {
                LLBMLPlugin.Log.LogDebug(i + ": " + inst);
                i++;
            }
        }
    }
#pragma warning restore 1591
}
