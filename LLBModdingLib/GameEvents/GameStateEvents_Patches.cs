﻿using System;
using System.Collections;
using HarmonyLib;
using LLBML.States;


namespace LLBML.GameEvents
{
#pragma warning disable 1591
    public static class GameStateEvents_Patches
    {
        /*
         * TODO this would be the proper way but it doesn' t work, shipping a workaround for now       
        [HarmonyPrefix]
        [HarmonyPatch(typeof(DNPFJHMAIBP), nameof(DNPFJHMAIBP.MLPLPAPJKGO), MethodType.Enumerator)] // GameStates.CSetState
        public static bool CSetState_Prefix(JOFJHDJHJGI __0, bool __1, DNPFJHMAIBP __instance, ref GameState __state)
        {
            LLBMLPlugin.Log.LogDebug("Event OnStateChange triggered. OldState: ");

            __state = GameStates.GetCurrent();
            return true;
        }

        [HarmonyPostfix]
        [HarmonyPatch(typeof(DNPFJHMAIBP), nameof(DNPFJHMAIBP.MLPLPAPJKGO), MethodType.Enumerator)] // GameStates.CSetState
        public static void CSetState_Postfix(JOFJHDJHJGI __0, bool __1, DNPFJHMAIBP __instance, ref GameState __state)
        {
            LLBMLPlugin.Log.LogDebug("Event OnStateChange triggered. OldState: ");

            GameStateEvents.OnStateChangeCall(__instance, new OnStateChangeArgs(__state, __0));
        }
        */
        [HarmonyPrefix]
        [HarmonyPatch(typeof(DNPFJHMAIBP), nameof(DNPFJHMAIBP.MLPLPAPJKGO), MethodType.Enumerator)] // GameStates.CSetState
        public static bool CSetState_Prefix(DNPFJHMAIBP __instance, ref GameState __state)
        {
            __state = GameStates.GetCurrent();
            return true;
        }

        [HarmonyPostfix]
        [HarmonyPatch(typeof(DNPFJHMAIBP), nameof(DNPFJHMAIBP.MLPLPAPJKGO), MethodType.Enumerator)] // GameStates.CSetState
        public static void CSetState_Postfix(DNPFJHMAIBP __instance, ref GameState __state)
        {
            GameState currentState = GameStates.GetCurrent();
            if (__state != currentState)
            {
                GameStateEvents.OnStateChangeCall(
                    __instance,
                    new OnStateChangeArgs(__state, currentState));
            }
        }
    }
#pragma warning restore 1591
}
